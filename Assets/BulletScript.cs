﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    Rigidbody2D _rb;
    public float bulletSpeed;
    private Vector2 _direction;

    private void Start() {
        _rb = GetComponent<Rigidbody2D>();
        Invoke("DestroySelf", 1f);
        _direction = PlayerController.LookingDirection;
    }

    private void FixedUpdate() {
        _rb.velocity = bulletSpeed * _direction;
    }

    void DestroySelf(){
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        PlayerController.HitEnemy(other);
        DestroySelf();
    }
}
