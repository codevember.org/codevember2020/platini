﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class EnemySync : MonoBehaviourPun //, IPunObservable
{
    bool oneTime = true;
    public bool IsAlive { get; set; }

    void Start()
    {
        IsAlive = true;
    }

    [PunRPC]
    void RPC_KillEnemy()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
    }

    //void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        //We own this player: send the others our data
    //        stream.SendNext(IsAlive);
    //    }
    //    else
    //    {
    //        //Network player, receive data
    //        IsAlive = (bool)stream.ReceiveNext();
    //    }
    //}

    void Update()
    {
        Debug.Log("isalive " + IsAlive);
        if (!IsAlive)
        {
            if (oneTime)
            {
                oneTime = false;
                photonView.RPC("RPC_KillEnemy", RpcTarget.All);
            }
        }
    }

}