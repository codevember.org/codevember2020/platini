﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{

    Rigidbody2D _rb;
    public float speed;
    [Tooltip("Do not change here")]
    public static Vector2 LookingDirection;

    Object bulletRef;

    public static void HitEnemy(Collision2D collision){
        Debug.Log("Enemy dead");
        // Destroy(collision.gameObject);
        //collision.transform.GetComponent<EnemySync>().IsAlive = false;
        //Destroy(collision.gameObject);
    }

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        LookingDirection = Vector2.up;
        bulletRef = Resources.Load("Bullet");
    }

    void Update()
    {
        Move();
        Shoot();
    }

    void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject bullet = Instantiate(bulletRef) as GameObject;
            bullet.transform.position = new Vector3(transform.position.x, transform.position.y);
        }
    }

    void Move()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            pos.y += speed * Time.deltaTime;
            LookingDirection = Vector2.up;
        }
        if (Input.GetKey(KeyCode.S))
        {
            pos.y -= speed * Time.deltaTime;
            LookingDirection = Vector2.down;
        }
        if (Input.GetKey(KeyCode.D))
        {
            pos.x += speed * Time.deltaTime;
            LookingDirection = Vector2.right;
        }
        if (Input.GetKey(KeyCode.A))
        {
            pos.x -= speed * Time.deltaTime;
            LookingDirection = Vector2.left;
        }

        transform.position = pos;
    }
}
