﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    private Vector3 currentVelocity = Vector3.zero;

    public float smoothTime = 20f;

    private void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player").transform;
        //transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
    }

    void FixedUpdate()
    {
        return;
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        } else
        {
            rolling();
        }
    }


    public void rolling()
    {
        Vector3 nextPosition = player.position;
        nextPosition.z = transform.position.z;
        transform.position = Vector3.SmoothDamp(transform.position, nextPosition, ref currentVelocity, smoothTime);
    }
}
