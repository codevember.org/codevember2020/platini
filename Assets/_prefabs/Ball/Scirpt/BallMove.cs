﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    private Rigidbody2D rigidbody;

    public float fDampingRegular;
    public float fDampingWhenKicked;
    public Vector2 stopAtWhenKicked;
    public Vector2 stopAt;
    public float fDampingWhenStopping;
    public bool kicked;

    public Sprite ballRegular;
    public Sprite ballKicked;

    public GameObject kickedBy;


    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float damping = 1f;
        
        if (kicked)
        {
            if (rigidbody.velocity.magnitude < stopAtWhenKicked.magnitude)
            {
                Unkick();
                damping *= Damp(fDampingWhenStopping);
            } else
            {
                damping *= Damp(fDampingWhenKicked);
                Debug.Log(rigidbody.velocity* damping);
            }
        }
        else if (rigidbody.velocity.magnitude < stopAt.magnitude)
        {
            damping *= Damp(fDampingWhenStopping);
        }
        else
        {
            damping *= Damp(fDampingRegular);
        }
        rigidbody.velocity = rigidbody.velocity * damping;

    }

    public void Kick(GameObject kickedBy, Vector2 direction)
    {
        kicked = true;
        this.kickedBy = kickedBy;
        GetComponent<SpriteRenderer>().sprite = ballKicked;
        rigidbody.velocity = direction;
    }
    public void Unkick()
    {
        kicked = false;
        kickedBy = null;
        GetComponent<SpriteRenderer>().sprite = ballRegular;
    }

    private float Damp(float dampingFactor)
    {
        return Mathf.Pow(1f - dampingFactor, Time.deltaTime);
    }
}
