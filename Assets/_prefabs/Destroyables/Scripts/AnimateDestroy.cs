﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateDestroy : MonoBehaviour
{
    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ball")
        {
            BallMove ball = collision.collider.GetComponent<BallMove>();
            if (ball.kicked)
            {
                animator.SetTrigger("Destroy");
            }
        }
    }
}
