﻿using System;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    private PlayerState state;


    public float fWalkingSpeed;
    public float fKickPower;
    public float fDamping;
    public float fDampingWhenTurning;
    public float fDampingWhenStopping;


    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        state = GetComponent<PlayerState>();
    }

    void FixedUpdate()
    {
        MovePlayer();
    }

    private void Update()
    {
        KickBall();
    }

    private void KickBall()
    {
        if (state.ball)
        {
            BallMove ball = state.ball.GetComponent<BallMove>();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Vector2 force = ball.transform.position - transform.position;
                ball.GetComponent<BallMove>().Kick(gameObject, fKickPower * force.normalized);
                transform.up = force.normalized;
            }
        }
    }

    private void MovePlayer()
    {
        Vector2 fVelocity = rigidbody.velocity;

        Vector2 inputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        fVelocity += fWalkingSpeed * inputVector;
        float damping = 1f;
        if (inputVector.magnitude < 0.1f)
            damping *= Mathf.Pow(1f - fDampingWhenStopping, Time.deltaTime);
        else if (Vector2.Dot(inputVector, fVelocity) <   0f)
            damping *= Mathf.Pow(1f - fDampingWhenTurning, Time.deltaTime);
        else
            damping *= Mathf.Pow(1f - fDamping, Time.deltaTime);
        fVelocity *= damping;

        rigidbody.velocity = fWalkingSpeed * inputVector;
        
        if (inputVector.magnitude > 0) 
            transform.up = fVelocity.normalized;
    }
}
