﻿using System;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public Transform outterCircle;
    public Transform innerCircle;

    public Sprite circleSuccess;
    public Sprite circleFail;
    public Sprite circleNeutral;
    public LayerMask whatIsBall;
    
    public Collider2D ball;

    void Start()
    {
        ball = GameObject.FindObjectOfType<BallMove>().GetComponent<Collider2D>();
    }

    void Update()
    {
        CanIKickIt();
    }

    private void CanIKickIt()
    {
        Vector2 capsuleScale = outterCircle.localScale;
        ball = Physics2D.OverlapCapsule(
            transform.position,
            capsuleScale,
            capsuleScale.x > capsuleScale.y
                ? CapsuleDirection2D.Horizontal
                : CapsuleDirection2D.Vertical,
            0,
            whatIsBall
        );
        if (ball)
        {
            BallMove ballMove = ball.GetComponent<BallMove>();
            if ((!ballMove.kicked || ballMove.kicked && ballMove.kickedBy.Equals(gameObject)))
            {
                innerCircle.GetComponent<SpriteRenderer>().sprite = circleSuccess;
                outterCircle.GetComponent<SpriteRenderer>().sprite = circleSuccess;
            }
            else
            {
                innerCircle.GetComponent<SpriteRenderer>().sprite = circleFail;
                outterCircle.GetComponent<SpriteRenderer>().sprite = circleFail;
            }

        }
        else
        {
            innerCircle.GetComponent<SpriteRenderer>().sprite = circleNeutral;
            outterCircle.GetComponent<SpriteRenderer>().sprite = circleNeutral;
        }
    }
}
